#!/bin/bash

# Create directory and password file
mkdir /home/blockchain
sudo chmod 777 /home/blockchain
mkdir /home/blockchain/pw
sudo chmod 777 /home/blockchain/pw
nano /home/blockchain/pw/pw.txt
# Enter your Prysm wallet password, hit CRTL+X, hit y, hit enter

# Generate JWT secret
openssl rand -hex 32 | sudo tee /home/blockchain/jwt.hex > /dev/null
