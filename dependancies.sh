#!/bin/bash

# Update and upgrade the system
sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y && sudo apt autoremove -y

# Install necessary packages
sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  git \
  ufw \
  openssl \
  lsb-release -y

# Configure firewall
sudo ufw allow 30303
sudo ufw allow 12000
sudo ufw allow 13000
sudo ufw allow 3000
sudo ufw allow 9090

# Install Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose

# Create directory and password file
cd ~
mkdir blockchain
sudo chmod 777 blockchain
cd blockchain
mkdir pw
sudo chmod 777 pw
cd pw
nano pw.txt
# Enter your Prysm wallet password, hit CRTL+X, hit y, hit enter
cd ~

# Generate JWT secret
openssl rand -hex 32 | sudo tee blockchain/jwt.hex > /dev/null

# Prompt for IP address
read -p "Enter your IP address: " ip_address

# Start Geth Docker container
sudo docker run -d \
  --name geth \
  --network=host \
  -p 30303:30303 \
  -p 8545:8545 \
  -v /home/ubuntu/blockchain:/blockchain \
  registry.gitlab.com/pulsechaincom/go-pulse:latest \
  --pulsechain \
  --authrpc.jwtsecret=/blockchain/jwt.hex \
  --datadir=/blockchain \
  --metrics \
  --pprof \
  --pprof.addr=0.0.0.0 \
  --pprof.port=6060

# Start Beacon Chain Docker container
sudo docker run -d \
--name beacon \
--network=host \
-p 4000:4000/tcp \
-p 12000:12000/udp \
-p 13000:13000/tcp \
-p 8080:8080/tcp \
-v /home/ubuntu/blockchain:/blockchain \
registry.gitlab.com/pulsechaincom/prysm-pulse/beacon-chain \
--pulsechain \
--jwt-secret=/blockchain/jwt.hex \
--datadir=/blockchain \
--checkpoint-sync-url=https://checkpoint.pulsechain.com \
--genesis-beacon-api-url=https://checkpoint.pulsechain.com \
--suggested-fee-recipient=0xA6d0522fC2926d2925bc99659c3d5614778ef774 \
--min-sync-peers=1 \
--p2p-host-ip="$ip_address" \
--monitoring-host=0.0.0.0 \
--monitoring-port=8080

git clone https://gitlab.com/pulsechaincom/staking-deposit-cli.git

cd staking-deposit-cli

sudo apt install python3-pip

sudo pip3 install -r requirements.txt

sudo python3 setup.py install

sudo ./deposit.sh install

# Configure container restart policy
sudo docker update --restart always geth
sudo docker update --restart always beacon