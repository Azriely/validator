# Start Validator Docker container
sudo docker run -it -v /home/ubuntu/blockchain/validator_keys:/keys \
-v /home/ubuntu/blockchain/pw:/wallet \
--name validator \
registry.gitlab.com/pulsechaincom/prysm-pulse/validator \
accounts import --keys-dir=/keys --wallet-dir=/wallet \
--pulsechain

sudo docker stop -t 180 validator

sudo docker container prune

sudo docker run -d \
--network=host \
-v /home/ubuntu/blockchain/validator_keys:/keys \
-v /home/ubuntu/blockchain/pw:/wallet \
--name validator \
registry.gitlab.com/pulsechaincom/prysm-pulse/validator \
--pulsechain \
--wallet-dir=/wallet \
--wallet-password-file=/wallet/pw.txt \
--monitoring-host=0.0.0.0 \
--monitoring-port=8081 \
--graffiti validators.azriel.io

# Configure container restart policy
sudo docker update --restart always validator

# Change permissions of validator deposit
cd ~/blockchain/validator_keys
# sudo chmod 777 deposit_data-**********.json